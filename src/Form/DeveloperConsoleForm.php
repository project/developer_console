<?php

namespace Drupal\developer_console\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\RowCountException;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Console form definition.
 */
class DeveloperConsoleForm extends FormBase {

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(
    RendererInterface $renderer,
    Connection $connection
  ) {
    $this->renderer = $renderer;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dev_console_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    $form['input_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Syntax'),
      '#options' => [
        'PHP' => $this->t('PHP code'),
        'SQL' => $this->t('DB query'),
      ],
      '#attributes' => ['class' => ['input-type-selector']],
      '#default_value' => $values['input_type'] ?? 'PHP',
    ];
    $form['input'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Code'),
      '#rows' => 20,
      '#default_value' => $values['input'] ?? '',
      '#attributes' => [
        'autocomplete' => 'off',
        'autocorrect' => 'off',
        'autocapitalize' => 'off',
        'spellcheck' => 'false',
      ],

    ];
    $form['save_entry'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('save in history'),
      '#default_value' => $values['save_entry'] ?? 1,
    ];
    $form['execute'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];

    $storage = $form_state->getStorage();
    if (isset($storage['results'])) {
      $form['result'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Results'),
      ];
      $form['result']['time'] = [
        '#markup' => $this->outputResult(
          $this->t('Execution time'),
          $this->t('@time miliseconds', ['@time' => $storage['results']['time']])
        ),
      ];
      if (isset($storage['results']['return'])) {
        $form['result']['return'] = [
          '#markup' => '<div>' . $storage['results']['return'] . '</div>',
        ];
      }
      if (isset($storage['results']['print'])) {
        $form['result']['print'] = [
          '#markup' => '<div>' . $storage['results']['print'] . '</div>',
        ];
      }
    }

    // Console history.
    $history_arr = [];
    $result = $this->connection->select('developer_console_history', 'h')->fields('h', [
      'hid',
      'type',
      'input',
    ])->orderBy('hid', 'DESC')->execute();
    while ($data = $result->fetchAssoc()) {
      $history_arr[$data['type']][] = [
        'hid' => $data['hid'],
        'input' => $data['input'],
      ];
    }

    $history_html = '<div class="console-history">';
    foreach ($history_arr as $type => $data) {
      $history_html .= '<ul id="console-history-' . $type . '">';
      foreach ($data as $element) {
        $history_html .= '<li><a href="javascript:void(0)" class="console-history-selector">+</a><pre class="input" style="display: inline-block;">' . $element['input'] . '</pre></li>';
      }
      $history_html .= '</ul>';
    }
    $history_html .= '</div>';

    $form['history'] = [
      '#markup' => $history_html,
    ];

    $form['#attached']['library'][] = 'developer_console/ui';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('input'))) {
      $form_state->setError($form['input'], $this->t('Input is required.'));
    }
  }

  /**
   * Helper function to eval code.
   *
   * Avoids accidently changing any local variables of the caller.
   *
   * @param string $code
   *   The code to eval.
   */
  protected function eval($code) {
    return eval($code);
  }

  /**
   * Helper function to output results.
   *
   * @param string $title
   *   The results title.
   * @param string $result
   *   The result.
   * @param bool $wrapper
   *   Should the result be wrapped in <p>?
   */
  protected function outputResult($title, $result, $wrapper = ['p']) {
    $output = '<h4>' . $title . '</h4>';
    $open = '';
    $close = '';
    if (!empty($wrapper)) {
      foreach ($wrapper as $tag) {
        $open .= '<' . $tag . '>';
        $close = '</' . $tag . '>' . $close;
      }
    }
    $output .= $open . $result . $close;
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild();

    $storage = [];

    // Code execution and timetracking.
    $input_type = $form_state->getValue('input_type');
    $input = $form_state->getValue('input');
    switch ($input_type) {
      case 'PHP':
        ini_set('display_errors', 'stderr');
        ob_start();

        $start_time = microtime(TRUE);
        try {
          $storage['results']['result'] = $this->eval($input);
        }
        catch (\ParseError $e) {
          $this->messenger()->addError($this->t('Parse error: @error. File: @file, Line: @line', [
            '@error' => $e->getMessage(),
            '@file' => $e->getFile(),
            '@line' => $e->getLine(),
          ]));
        }
        catch (\Exception $e) {
          $this->messenger()->addError($this->t('Uncaught exception: "@message" Thrown in @file, Line: @line', [
            '@message' => $e->getMessage(),
            '@file' => $e->getFile(),
            '@line' => $e->getLine(),
          ]));
          kdpm($e->getTrace());
        }
        $end_time = microtime(TRUE);

        $storage['results']['print'] = ob_get_contents();
        ob_end_clean();
        break;

      case 'SQL':
        $start_time = microtime(TRUE);
        try {
          $options = [
            'fetch' => \PDO::FETCH_OBJ,
            'allow_delimiter_in_query' => FALSE,
            'allow_square_brackets' => FALSE,
            'pdo' => [],
            'debug' => TRUE,
          ];
          $result = $this->connection->prepareStatement($input, $options, TRUE);
          $result->execute([], $options);
        }
        catch (Exception $e) {
          $result = kdpm($e, 'SA');
        }
        $end_time = microtime(TRUE);
        break;
    }

    // Process results.
    switch ($input_type) {
      case 'PHP':
        if (isset($storage['results']['return'])) {
          $storage['results']['return'] = $this->outputResult($this->t('Returned value:'), $storage['results']['return']);
        }
        if (isset($storage['results']['print'])) {
          $storage['results']['print'] = $this->outputResult(
            $this->t('Printout:'),
            $storage['results']['print'],
            ['p', 'pre']
          );
        }
        break;

      case 'SQL':
        if (is_object($result)) {
          $rows = [];
          foreach ($result as $row) {
            $rows[] = (array) $row;
          }
          try {
            $row_count = $result->rowCount();
          }
          catch (RowCountException $e) {
            $row_count = 0;
          }
          $storage['results']['return'] = $this->outputResult($this->t('Query result:'), $this->t('Affected rows: @rows', [
            '@rows' => $row_count,
          ]));
          $storage['results']['print'] = $this->table($rows);
        }
        else {
          $storage['results']['print'] = $this->outputResult($this->t('Error:'), $result, ['pre']);
        }
        break;
    }
    $storage['results']['time'] = round(($end_time - $start_time) * 1000, 3);

    // Record input in history.
    if ($form_state->getValue('save_entry')) {
      $nmax = 10;
      do {
        $ntype = $this->connection->select('developer_console_history', 'h')
          ->fields('h', ['hid'])
          ->condition('type', $input_type)
          ->countQuery()
          ->execute()
          ->fetchField();

        if ($ntype >= $nmax) {
          $hid = $this->connection->select('developer_console_history', 'h')
            ->fields('h', ['hid'])
            ->condition('type', $input_type)
            ->orderBy('hid', 'ASC')
            ->range(0, 1)
            ->execute()
            ->fetchField();

          $this->connection->delete('developer_console_history')->condition('hid', $hid)->execute();
        }
      } while ($ntype >= $nmax);

      // Avoid saving identical entries multiple times.
      $last_entry = $this->connection->select('developer_console_history', 'h')
        ->fields('h', ['input'])
        ->condition('type', $input_type)
        ->orderBy('hid', 'DESC')
        ->range(0, 1)
        ->execute()
        ->fetchField();

      if ($last_entry != $input) {
        // Save history item.
        $this->connection->insert('developer_console_history')->fields([
          'type' => $input_type,
          'input' => $input,
        ])->execute();
      }
    }

    $form_state->setStorage($storage);
  }

  /**
   * Helper function to render results table.
   */
  protected function table($data) {
    $renderable = [
      '#theme' => 'table',
      '#title' => $this->t('Results:'),
      '#header' => [],
      '#rows' => [],
      '#empty' => $this->t('No results.'),
    ];

    if (!empty($data)) {
      foreach ($data[0] as $key => $obsolete) {
        $renderable['#header'][$key] = $key;
      }

      foreach ($data as $key => $row) {
        foreach ($renderable['#header'] as $title) {
          $renderable['#rows'][$key][]['data'] = $row[$title] ?? '';
        }
      }
    }

    return $this->renderer->render($renderable);
  }

}
