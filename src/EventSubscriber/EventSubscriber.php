<?php

namespace Drupal\developer_console\EventSubscriber;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The event subscriber class.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $routeMatch
   *   The route match service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    private readonly RouteMatchInterface $routeMatch,
    private readonly StateInterface $state
  ) {
  }

  /**
   * Display debug information if needed.
   */
  public function displayRouteInformation(RequestEvent $event): void {
    if ($this->state->get('dev.path_info', FALSE) !== TRUE) {
      return;
    }

    $request = $event->getRequest();
    kdpm([
      'route name' => $this->routeMatch->getRouteName(),
      'request uri' => $request->getRequestUri(),
      'query string' => $request->getQueryString(),
      'method' => $request->getRealMethod(),
      'content type' => $request->getContentTypeFormat(),
      'body' => $request->getContent(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['displayRouteInformation'];
    return $events;
  }

}
